module SetupNative where

import System.Directory
import System.FilePath
import PowerShell
import Control.Monad

dirs :: [String]
dirs = ["bin", "include", "scripts", "temp"]

setup :: IO ()
setup = do
    dataDir <- getAppUserDataDirectory "native-libs"
    mapM_ (\d -> createDirectoryIfMissing True (dataDir </> d)) dirs
    let binPath     = dataDir </> "bin"
        includePath = dataDir </> "include"
    putStrLn $ "Adding " ++ binPath ++ " to the LIBRARY_PATH environment variable and"
    putStrLn $ "adding " ++ includePath ++ " to the CPATH environment variable."
    setupEnvVars
    putStrLn $ "It's recommended that you also add " ++ binPath ++ " to your PATH variable."
    putStrLn "Would you like this to be done for you? (y/n)"
    shouldAddPath <- getYN
    when shouldAddPath addPath

getYN :: IO Bool
getYN = do
    line <- getLine
    case line of
        "y" -> return True
        "Y" -> return True
        "n" -> return False
        "N" -> return False
        _ -> putStrLn "Please answer with y or n." >> getYN
