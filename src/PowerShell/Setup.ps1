$newCPath = $env:CPATH + ";" + $env:APPDATA + "\native-libs\include"
[Environment]::SetEnvironmentVariable("CPATH", $newCPath, [System.EnvironmentVariableTarget]::User)
[Environment]::SetEnvironmentVariable("CPATH", $newCPath, [System.EnvironmentVariableTarget]::Process)
$newLibPath = $env:LIBRARY_PATH + ";" + $env:APPDATA + "\native-libs\bin"
[Environment]::SetEnvironmentVariable("LIBRARY_PATH", $newLibPath, [System.EnvironmentVariableTarget]::User)
[Environment]::SetEnvironmentVariable("LIBRARY_PATH", $newLibPath, [System.EnvironmentVariableTarget]::Process)
