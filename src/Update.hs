module Update where

import PowerShell
import System.FilePath
import System.Directory
import Control.Monad

update :: IO ()
update = do
    list <- filter (any (/= ' ')) . lines
        <$> download "https://gitlab.com/LukaHorvat/native/raw/master/libs.txt"
    forM_ list $ \s -> do
        let name = reverse . takeWhile (/= '/') . reverse $ s
        dataDir <- getAppUserDataDirectory "native-libs"
        download s >>= writeFile (dataDir </> "scripts" </> name)
