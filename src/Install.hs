module Install where

import System.FilePath
import PowerShell
import System.Directory
import Text.Printf

install :: String -> String -> IO ()
install lib args = do
    dataDir <- getAppUserDataDirectory "native-libs"
    let tempDir    = dataDir </> "temp"
        scriptsDir = dataDir </> "scripts"
        includeDir = dataDir </> "include"
        binDir     = dataDir </> "bin"
        dirArgs = printf "-temp \"%s\" -scripts \"%s\" -include \"%s\" -bin \"%s\" "
                         tempDir scriptsDir includeDir binDir
    runPSStream (scriptsDir </> lib <.> "ps1") (dirArgs ++ args)
    cleanTemp
