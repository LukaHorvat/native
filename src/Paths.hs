module Paths where

import System.FilePath

dirs :: [String]
dirs = ["bin", "include", "scripts", "temp"]

binPath :: String
binPath     = homeDir </> "bin"

includePath :: String
includePath = homeDir </> "include"
