module Main where

import Options.Applicative
import SetupNative
import Install
import Update

data Action = Update | Setup | Install String String deriving (Eq, Ord, Read, Show)

extraOptsParser :: String -> Either String Char
extraOptsParser "" = Left "No characters left to parse"
extraOptsParser (c : _) = Right c

installParser :: Parser Action
installParser = Install <$> strArgument (metavar "LIBRARY")
                        <*> (unwords <$> many (strArgument (metavar "ARGS...")))

actionParser :: Parser Action
actionParser = subparser (
       command "update" (info (pure Update) (progDesc updateDesc))
    <> command "setup" (info (pure Setup) (progDesc setupDesc))
    <> command "install" (info installParser (progDesc installDesc)))
    where setupDesc = "Create the folders for libraries and set environment variables"
          installDesc = "Install a library"
          updateDesc = "Update the available library list"

main :: IO ()
main = do
    act <- execParser (info actionParser fullDesc)
    case act of
        Update -> update
        Setup -> setup
        Install lib args -> install lib args
