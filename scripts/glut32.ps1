param([String]$tempDir, [String]$binDir, [String]$includeDir)
echo "`n`n`n"
echo "Downloading glut32..."
wget "http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MinGW.zip" -out ($tempDir + "\glut.zip")
echo "Unpacking..."
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip ($tempDir + "\glut.zip") ($tempDir + "\glut")
echo "Copying..."
$glutBin = $tempDir + "\glut\freeglut\bin\x64"
move-item ($glutBin + "\freeglut.dll") ($binDir + "\glut32.dll")
echo "Done!"
