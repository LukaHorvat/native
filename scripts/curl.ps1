param([String]$tempDir, [String]$binDir, [String]$includeDir, [String]$z="7z")
if ((Get-Command $z -ErrorAction SilentlyContinue) -eq $null)
{
    echo "7z not in PATH and/or the supplied 7z doesn't exist. Looking in Program Files..."
    $z = "C:\Program Files\7-Zip\7z.exe"
}
if ((Get-Command $z -ErrorAction SilentlyContinue) -eq $null)
{
    echo "Not in Program Files. Trying Program Files (x86)..."
    $z = "C:\Program Files (x86)\7-Zip\7z.exe"
}
if ((Get-Command $z -ErrorAction SilentlyContinue) -eq $null)
{
    $Host.UI.WriteErrorLine("Can't find 7z anywhere.")
    $Host.UI.WriteErrorLine("Please make sure you have 7zip installed and 7z available in your path.")
    $Host.UI.WriteErrorLine("Alternatively, you can supply the path to 7z with a command line option like this:")
    $Host.UI.WriteErrorLine("`n    native install curl -- -z INSERT_PATH_TO_7Z_HERE.exe")
    exit
}
echo "`n`n`n"
echo "Downloading curl..."
wget "http://curl.haxx.se/gknw.net/7.40.0/dist-w64/curl-7.40.0-devel-mingw64.7z" -out ($tempDir + "\curl.7z")
echo "Unpacking..."
$cmd = "& '$z' " + "x '" + $tempDir + "\curl.7z' " + "-o'" + $tempDir + "\curl'"
invoke-expression $cmd
echo "Copying..."
$curlDir = $tempDir + "\curl\curl-7.40.0-devel-mingw64"
$curlBin = $curlDir + "\bin"
move-item ($curlBin + "\curl.exe") $binDir
move-item ($curlBin + "\libcurl.dll") $binDir
move-item ($curlDir + "\include\curl") $includeDir
echo "Done!"
